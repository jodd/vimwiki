super + Esc         restart


bspc query -N -n .(tab-complete)
	above	floating	locked	sticky
	active	focused	marked	tiled
	osv,
# query for active flags

dropdownname="dropdown"
bspc query -N -n .hidden >/dev/null || setsid $TERMINAL -n "$dropdownname" -e dropdown >/dev/null 2>&1 &
bspc rule -a St:$dropdownname hidden=on
# see bspwmrc for rules and setting flag.

super + w
    notify-send "Msg..."
super + @w
    start program
# @will delay action until released



