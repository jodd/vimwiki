#   find
#   fd


:   rm files found
find /home/jan/tmp -type f -exec rm {} +

:   search for duplicates
find /home/jan/tmp -name *.jpg -exec mmdum {} + | sort | uniq --all-repeated=separate -w 15 > tmp-dupes.txt

:   
find . -name "*.jpg" | cat -n | while read n f; do mv "$f" "$n-toh.jpg";done

:   chmod..
find ~/tmp -type d -exec chmod 755 {} +
find ~/tmp -type f -exec chmod 644 {0 +


find ~/ -type f ! -name "*.jpg"		# find all exept for...


find Music -type f ! -name "*mp3" ! -name "*.m4a"	#Utelukker mp3 og m4a
find Music -not -name NYMAPPE -exec mv -t NYMAPPE {} +

:   searching broken links
find . -type l

:   searching specific groups:
find / -type f -group users 

# ======================== #

search for CR2 files and copy ...works when all / \ and ; are in place:
find /hdd/mink/TRETERRAID/Pictures/jan/2019 -iname "*.CR2*" -exec cp {} /data/photos/2019/CR2/ \;


