:Goyo       to start
:Goyo!      to quit
:Goyo [700x400]

The window can be resized with the usual 

[count]<CTRL-W> + >, <, +, - keys,


and <CTRL-W> + = will resize it back to the initial size.
