#   ssh

ssh jan@mira
ssh-keygen
ssh-keygen -t ecsda -b 521
ssh-keygen -R 192.168.1.108	#Vil fjerne host fra known_hostname
ssh-copy-id -i [-p 22]  ~/.ssh/id_rsa.pub jan@192.168.1.108
ssh-keygen -p	#For å fjerne passord:	/etc/ssh/sshd_config

## Notes to remember and understand ssh

For each place I will use ssh, I'll make an specific ssh key.


# recommended, standard.
ssh-keygen -t ecdsa -b 521 -C "Comment"

# make a name for each...
ssh-keygen -t ecdsa -b 521 -C "Standard"
(.ssh/id_ecdsa_github, id_ecdsa_main, etc)

# copy to server
ssh-copy-id -i ~/.ssh/id_ecdsa_server.pub user@server

# then add
ssh-add ~/ssh/id_ecdsa_server


# github
vim ~/.ssh/config
	Host github.com
		Hostname github.com
		User joddbeem 
		Port 22
		IdentityFile ~/.ssh/id_ecdsa_github


# /etc
/etc/ssh/ssh_config		-->		Port xxx

/etc/ssh/sshd_config	-->		Port, AddressFamily, PasswordAuthentication, 

#
known_host=correct computer
authorized_keys=correct user
The known_hosts file is a triplet of the machine name, its IP address, and its public key

ssh-keygen -t ed25519 -f ~/.ssh/[filename] -C "[useful comment]"
ssh-copy-id -i ~/.ssh/[filename].pub jan@10.0.0.47

if not able to connect: Authentication Failures
ssh -p 22 -o IdentitiesOnly=yes 10.0.0.9
https://www.tecmint.com/fix-ssh-too-many-authentication-failures-error/


