#   scp

scp /path/to/file username@a:/path/to/destination
scp username@b:/path/to/file /path/to/destination
scp -r /directory/ user@ip-adress:/home/user/Downloads
