== colorpicker ==
# tool to find a on-screen color


Usage:
  colorpicker [OPTION…]

Help Options:
  -h, --help               Show help options
  --help-all               Show all help options
  --help-gtk               Show GTK+ Options

Application Options:
  --short                  Use #RRGGBB output format
  --one-shot               Exit after picking one color
  --preview                Show the current color in preview rectangle
  --display=DISPLAY        X display to use

## example ##

colorpicker --one-shot --preview

prints out:
R: 122, G: 166, B: 218 | Hex: #7AA6DA
and then it quits
