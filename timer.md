
timer (systemctl alternative cron)
/usr/lib/systemd/system/fil.timer
/usr/lib/systemd/system/fil.service
/home/jan/mon/script/fil.sh
https://wiki.archlinux.org/index.php/Systemd/Timers#See_also
Må lage script-fil, må lage en .timer og en .service
og en som gjør hva som skal gjøres...


https://fedoramagazine.org/systemd-timers-for-scheduling-tasks/

https://opensource.com/article/20/7/systemd-timers

cron, anacron or systemd timers

I want to understand the latter.

A script that runs whatever:


#!/bin/bash
echo "This is a test: $(date)" >> "~/tmp/test_log"

@ ~/.local/scripts/systemd/Test_timer.sh


A unit that will call the above script:

[Unit]
Description=A test of systemd scheduler

[Service]
Type=simple
ExecStart=/home/jan/.local/scripts/systemd/Test_timer.sh

[Install]
WantedBy=default.target

@ ~/.config/systemd/Test_timer.service


The its time for timer

[Unit]
Description=Test timer every 1 minute
RefuseManalStart=no
RefuseManualStop=no

[Timer]
# If machine is of, execute when running again:
Persitent=true
OnBootSec=120
OnUnitAciveSec=60
Unit=Test_timer.service

[Install]
WantedBy=timers.target

@ .config/systemd/Test_timer.timer

OnBootSec
OnUnitActive

OnCalendar=*-*-* *:*:00       Every minute, every day

OnCalendar=*-*-* *:00:00      Every hour, every day

OnCalendar=*-*-* 00:00:00     Every day

OnCalendar=*-*-1,15 12:10:00  runs 1st and 15th @ 12:10

OnCalendar=Mon,Tue,Wed,Thu,Fri 2021-*-16 12:20:00 Runs the 16th if its a weekday


systemctl --user enable Test_timer.service
(/home/jan/.config/systemd/Test_timer.service)

systemctl --user start Test_timer.service
systemctl --user stop Test_timer.service

To enable timer:

systemctl --user enable Test_timer.timer
systemctl --user start Test_timer.timer





