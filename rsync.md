#   rsync


rsync --recursive \
    --archive \
    --verbose \
    --update \
    --checksum \
    --safe-links \
    --rsh=ssh /home/jan/.config \
    --exclude 'BraveSoftware' \
    jan@mink:/home/jan/TRETERRAID/Name-of-backup-file
    
rsync -ravucle ssh /home/jan/.config --exclude 'BraveSoftware' jan@mink:/home/jan/TRETERRAID/Name-of-backup-file



rsync -v -e ssh /home/jan/test-rsync/* jan@mira:/home/jan/test-rsync	#tar ikke med mapper, tar alt


rsync -u -v -e ssh /home/jan/test-rsync/* jan@mira:/home/jan/test-rsync	#oppdaterer


rsync -u -v -r -e ssh /home/jan/test-rsync/* jan@mira:/home/jan/test-rsync #tar med det i mapper, dropp '/*' for hele directory med innhold


rsync -u -v -r -e ssh /home/jan/test-rsync/ jan@mira:/home/jan/test-rsync #


rsync -u -v -r -l -e ssh 	copy symlinks as symlinks	

## this command can be used to synchronize a folder
rsync -avxHAX --progress / /new-disk/

-a  : all files, with permissions, etc..
-v  : verbose, mention files
-x  : stay on one file system
-H  : preserve hard links (not included with -a)
-A  : preserve ACLs/permissions (not included with -a)
-X  : preserve extended attributes (not included with -a)
