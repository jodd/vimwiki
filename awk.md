# awk -F/         delimiter is /
  awk -F'[ /]     delimiter is space or /
  
  ip route | awk -F'[ /]' '{ print $1 }'
> default
> 10.0.0.0

  ip route | awk -F'[ /]' 'NR==2 { print $1; exit }'
> 10.0.0.0

##################################

examples:
xwininfo -root -tree | awk 'BEGIN {IGNORECASE = 1} /polybar/ { print }'

sudo blkid | awk -F\" '/nvme/ { print $2 }'

