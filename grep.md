grep -v     Reverse (don't show)

grep -ve '^#' -ve '^$' ~/.config/sxhkd/sxhkdrc

=

grep -vE '^(#|$)' ~/.config/sxhkd/sxhkdrc

[   will remove lines beginning wiht # or a newline




.   Any one character
*   Match any number of previous, including 0
+   Match any number of previous
?   Previous character is optional
$   End of the line
^   Beginning of the line
\S  Any non-whitespace character
\s  any whitespace character
[a-z]
[A-Z]
[A-Za-z]    Any letter

\       Escape character


grep -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' /etc/hosts

    = outputs ip addresses

grep -E -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' /etc/hosts

    = only the matched numbers
    = {1,3} defines that it has to be 1, 2 or 3 digits
    
