Q           quitall
qq          cd ~

M   linemode:
o   sorting:
    
t   tag_toggle
ut  tag_remove

### search ###

/   search
n   search next
N   search previous
c   search:



## console alias ##


e           edit
q           quit
q!          quit!
qa          quitall
qa!         quitall!
qall        quitall
qall!       quitall!
setl        setlocal

filter      scout -prt
find        scout -aeit
mark        scout -mr
unmark      scout -Mr
search      scout -rs
search_inc  scout -rts
travel      scout -aefklst

