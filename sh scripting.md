
-eq, -ne, -lt, -le, -gt, -ge
-z, -n
==, !=
-a


### binaries
  -eq (arg1 is equal to arg2)
  -ne (arg1 is not equal to arg2)
  -lt (arg1 is less than arg2)
  -le (arg1 is less than or equal to arg2)
  -gt (arg1 is greater than arg2)
  -ge (arg1 is greater than or equal to arg2)

### string operators
  -z string (length of string is zero)
  -n string (length of string is non-zero)
  string1 = string2 (strings are equal)
  string1 != string2 (string are not equal)
  string1 < string2 (string1 sorts before string2)
  string1 > string2 (string1 sorts after string2)

### file operators
  -a file (file exists)
  -b file (file exists and is a block special)
  -c file (file exists and is a character special)
  -d file (file exists and is a directory)
  -e file (file exists)
  -f file (file exists and is a regular file)
  -g file (file exists and is set-group-id)
  -h file (file exists and is a symbolic link)
  -k file (file exists and is sticky)
  -p file (file exists and is a named pipe)
  -r file (file exists and is readable)
  -s file (file exists and has a size greater than zero)
  -t fd   (file descriptor is open and refers to a terminal)
  -u file (file exists and is set-user-id)
  -w file (file exists and is writable)
  -x file (file exists and is executable)
  -O file (file exists and is owned by the current user)
  -G file (file exists and is owned by the current user group)
  -L file (file exists and is a symbolic link)
  -S file (file exists and is a socket)
  -N file (file exists and has been modified since last read)
  file1 -nt file2 (file1 is newer by modification date than file2)
  file1 -ot file2 (file1 is older by modification date than file2)
  file1 -ef file2 (file1 and file2 have the same device and inode)

### conditional logic
  if [ $opt -eq 1 ]; then
  HOST="www.joedog.org"
  elif [ $opt -eq 2 ]; then
  HOST="ftp.joedog.org"
  else
  HOST="ssl.joedog.org"
  fi

##### to play it safe, same as above:
  if [ ${opt:-0} -eq 1 ] ; then
    HOST="www.joedog.org"
  elif [ ${opt:-0} -eq 2 ] ; then
    HOST="ftp.joedog.org"
  else
    HOST="ssl.joedog.org"
  fi

### 
  && - logical AND  if [ condition1 ] && [ condition2 ]
  || - logical OR   if [ condition1 ] || [ condition2 ]
  !  - logical NOT  if [ ! condition ]

################## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111 #####################


