# = Stikkord, commandoer jeg av og til glemmer: =

[[awk]]
bash  zsh   chsh -s / check: ps -p $$
[[bspwm]]
[[chattr]]
[[chown]]
[[colorpicker]]
[[colors]]
[[convert](convert)] imagemagic
[[cups]] printer, hplip, lpc 
[[curl]] curl -O to save a file
[[cut]]
[[dd]]       dd if=~/Downloads/image.iso of=/dev/sdc
[[dev_null]]
[[du]] or: ncdu 
[[dunst]] notify-send dunstfy
[[find]] /home/jan/tmp -type f -name "yoga" [[fd]]
[[fonts]]
[[git]] git add --all git push -u origin master
[[gpg]]
[[grabc]]
[[grep]] or even better: ripgrep: rg
[[hdparm]]
[[msensors]]
[[lvm]]
[[lsblk	]]viser mulige mountpoints
[[mdadm]]
[[mkdir]]
[[mogrify]] imagemagic
[[mpv]]
[[mv]]
[[nvme]]
[[pacman]] #yay [[paru]] [[makepkg]] diff make install clean
[[polybar]]
[[printf]]
[[qtile]]
[[ranger]] filemanager
[[rename]]
[[rsync]]
[[samba]]
[[scp]]	#To copy a file from B to A while logged into A:
[[sed]] -n "s/word/noword/g" tekstfile
[[setxkbmap]] -model pc 105 -layout no; numlockx on (must be seperate installed)
[[shred]] -zuv small/*
[[sh scripting]]
[[ssh]]
[[sshfs]]
[[suckless]] dmenu st patching 
[[sxhkd]]
[[systemctl]]
[[systemctl|journalctl]]
[[tar]]
[[timedatctl]] time date 
[[timer]]
[[tmux]]
[[updog]]
[[useradd]] 
[[vimdiff]] or nvim -d
[[virtualbox]]
[[wget]] https://www.something
[[x11 ]] xorg X11 xinitrc startx
[[xclip]]
[[xdg]] xdg-open xdg-mime 
[[xmodmap]]
[[xwininfo]] xev 
[xrandr](xrandr)
blkid	viser UUID
dmesg
lsmod
lxappearance
modprobe -h
uname -a
unig --all-repeated=separate -w 15	# comparing 15 characters, separate finds with space
| sort		# sorting from pipe
| uniq		# comparing values


