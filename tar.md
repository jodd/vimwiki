#   tar compressing and unpacking

tar -xvf package_name.tar.gz
tar -xvfz
tar xvf xxx.tar.gz
tar xvf xxx.tar.gz -C /home/jan/mappe
tar

tar -xvf dmenu-4.9.tar.gz
cd dmenu-4.9
sudo make install

patch -p1 < dmenu_hightlight-4.9.diff
sudo make install
