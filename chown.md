#   chown

chown jan:jan Folder/
chown jan:jan Folder/*
chmod +x ./File
chmod ug+wx Filename
chmod o+x Filename
chmod 644 Filename
