#   vim-surround

cs'"        change ' > "
ysiw'       surround word
yss'        surround line
ds'         remove ' surrounding

https://github.com/tpope/vim-surround
ChangeSurrounding           cs"'    "test" > 'test'
cs"<q>  > <q>test</q>

This line is changed by
yss)
(This line is changed by)

yss(
( This line is changed by )


ChangeTags
cst
dst

DeleteTags



# "exsample"
# '# I will try line'
