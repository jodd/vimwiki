#   packman

Return to an earlier package version

/var/cache/pacman/pkg/
pacman -U /var/cache/pacman/pkg/package-old_version.pkg.tar.xz
IgnorePkg=linux @ pacman.conf

packman
sudo ls /var/cache/pacman/pkg/ | wc -l
du -sh /var/cache/pacman/pkg/ 
sudo paccache -r    remove packages, leaves 3 of each
-d  --dryrun
-r  --remove
-k  --keep <num>
-u  --uninstalled
paccache -ruk0      remove all uninstalled packages


pacman -Qi linux
pacman -Qi linux-lts
-D  --database
-Q  --query
-R  --remove
-S  --sync
-T  --deptest
-U  --upgrade
-F  --files
-V  --version
-h  --help
-b  --dbpath <path>
-r  --root <path>
-v  --verbose

